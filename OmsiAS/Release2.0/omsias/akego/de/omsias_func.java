package omsias.akego.de;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

public class omsias_func {

	private Properties prop = new Properties();
	private FileOutputStream output = null;
	private FileInputStream input = null;
	private String propfilename = "config.properties";
	private String propfilepath;
	public String curversion = "2.0"; // current version
	
	//
	//
	//choosers (used to choose OmsiDir aswell as paths to .exe-Files)
	//
	//
	public String filechooserDir(){
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY );
		fc.showOpenDialog(null);
		fc.setDialogTitle("Choose Omsi Directory:");
		String placeoffile = fc.getSelectedFile().getAbsolutePath();
		return placeoffile;
	}
	
	public String filechooserEXE(){
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode( JFileChooser.FILES_ONLY );
		FileFilter filter = new FileNameExtensionFilter("Executable .exe","exe");
		fc.setFileFilter(filter);
		fc.showOpenDialog(null);
		fc.setDialogTitle("");
		String placeoffile = fc.getSelectedFile().getAbsolutePath();
		return placeoffile;
	}
	
	//
	//
	//	Properties saving and loading
	//
	//save properties
	public void saveProp() {
		File ftest = new File(getJarLoc()+"\\conf");
		if(!ftest.exists()){
			ftest.mkdirs();
		} 
		try {		
			output = new FileOutputStream(confLoc());
			// set the properties value
			prop.setProperty("omsidirpath", omsias_mainwindow.omsidir);
			prop.setProperty("hofsuiteexepath", omsias_mainwindow.pathhofsuite);
			prop.setProperty("blenderexepath", omsias_mainwindow.pathblender);
			prop.setProperty("setting_closeomsiasonomsistart", omsias_mainwindow.sett_closeomsias.toString());
			prop.setProperty("language", omsias_mainwindow.locale);
			prop.setProperty("downloadlink_omsisdk", omsias_mainwindow.downl_omsisdk);
			prop.setProperty("downloadlink_maptools", omsias_mainwindow.downl_maptools);
			prop.setProperty("downloadlink_hofsuite", omsias_mainwindow.downl_hofsuite);
			// save properties to project root folder
			prop.store(output, null);		
			output.close();
		} catch (IOException io) {
			io.printStackTrace();
		}
	}
	
	//load properties (especially the config.prop... where paths are saved)
	public void loadProp() {
		try {
			input = new FileInputStream(confLoc());
    		prop.load(input);
            omsias_mainwindow.omsidir = prop.getProperty("omsidirpath");
            omsias_mainwindow.pathhofsuite = prop.getProperty("hofsuiteexepath");
            omsias_mainwindow.pathblender = prop.getProperty("blenderexepath");
            omsias_mainwindow.sett_closeomsias = Boolean.parseBoolean(prop.getProperty("setting_closeomsiasonomsistart"));
            omsias_mainwindow.locale = prop.getProperty("language");
            omsias_mainwindow.downl_omsisdk = prop.getProperty("downloadlink_omsisdk");
            omsias_mainwindow.downl_maptools = prop.getProperty("downloadlink_maptools");
            omsias_mainwindow.downl_hofsuite = prop.getProperty("downloadlink_hofsuite");
            input.close();
            
    	} catch (IOException ex) {
    		ex.printStackTrace();

        }
	}
	
	public void deleteProp() {
		String confloc = new StringBuilder(getJarLoc()).append("\\conf\\config.properties").toString();
		File f = new File(confloc);
		f.delete();
	}
	
	//
	//get location of the jar file (the program) ´
	//
	public String getJarLoc() {
		File jarDir = new File(ClassLoader.getSystemClassLoader().getResource(".").getPath());
		String jarPath = jarDir.getAbsolutePath();
		jarPath = jarPath.replace("%20", " ");
		jarPath = jarPath.replace("%5c", "\\");
		return jarPath;
	}
	
	private String confLoc() {
		propfilepath = getJarLoc()+"\\conf\\"+propfilename;
		return propfilepath;
	}

	//
	//
	//	UpdateChecker
	//
	//
	public String onUpdateChecker() {
		String retstr = omsias_mainwindow.bundl.getString("omsias.output.currentomsiasversion")+" "+curversion;
		openurl("https://gitlab.com/omsi2/OmsiAS/blob/master/updatechecker_%23noedit");
		return retstr;
	}
	
	//
	//	open link in browser
	//
	public void openurl(String pgurl) {
		Desktop desktop = java.awt.Desktop.getDesktop();
		File f = new File(pgurl);
		URI _url;
		try {
			_url = new URI(pgurl);
			desktop.browse(_url);
		} catch (URISyntaxException | IOException e) {
			//if the url is not a valid url (which can be opened via browser)
			try {
				_url = f.toURI();
				desktop.browse(_url);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	//
	//	open explorer
	//
	public void openExplorer(String dirpath) {
		try {
			Desktop.getDesktop().open(new File(dirpath));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//
	//	start process
	//
	public void launchprogram(String path) {
		try {
	        new ProcessBuilder(path).start();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	public void launchprogramwparams(String path) {
		try {
	        Runtime r = Runtime.getRuntime();
	        r.exec(path);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	//
	//	check if file exists
	//
	public Boolean fileExists(String pathtofile) {
		File f = new File(pathtofile);
		Boolean retBoo = f.exists();
		return retBoo;
	}
	
	//
	//	read logfile line by line and get all Error Messages
	//
	public String readLogLine(String pathtologfile) {
		File file = new File(pathtologfile);
		String retstr = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\tCuttet Logfile, made with OmsiAS\n\t   Warnings and Errors only\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
		Scanner scanner;
		try {
			scanner = new Scanner(file);
			//now read the file line by line...
			int lineNum = 0;
			while (scanner.hasNextLine()) {
			    String line = scanner.nextLine();
			    lineNum++;
			    if(line.contains("Error:")||line.contains("Warning:")) {   
			    	retstr = new StringBuilder(retstr).append("\n[Line:"+lineNum+"]\t"+line).toString();
			    }
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			retstr = "ERROR";
		}
		return retstr;
	}
}