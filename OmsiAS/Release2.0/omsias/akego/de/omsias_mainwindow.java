package omsias.akego.de;
import java.io.File;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Separator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class omsias_mainwindow extends Application {

	//language declarations
	public static Locale curlang;
	public static ResourceBundle bundl;
	
	//window declarations
	BorderPane bp = new BorderPane();
	ImageView iv_header, iv_ico_omsistart, iv_ico_gbpatch, iv_ico_patchchanger, iv_ico_omsieditor, iv_ico_logfileerrorcut, iv_ico_hofsuite, iv_ico_maptools, iv_ico_omsixconv, iv_ico_streetcre, iv_ico_objeditp, iv_ico_blender;
	Image img_header = new Image(omsias_mainwindow.class.getResourceAsStream("/img/img_header.png"));
	Image img_ico_omsistart = new Image(omsias_mainwindow.class.getResourceAsStream("/img/icon_startomsi2.png"));
	Image img_ico_editorstart = new Image(omsias_mainwindow.class.getResourceAsStream("/img/icon_startomsieditor.png"));
	Image img_ico_gppatch = new Image(omsias_mainwindow.class.getResourceAsStream("/img/icon_ntcore_4gbpatch.png"));
	Image img_ico_patchchanger = new Image(omsias_mainwindow.class.getResourceAsStream("/img/icon_patchchanger_tram.png"));
	Image img_ico_logferrorcut = new Image(omsias_mainwindow.class.getResourceAsStream("/img/icon_logferrorcut.png"));
	Image img_ico_hofsuite = new Image(omsias_mainwindow.class.getResourceAsStream("/img/icon_hofsuite.png"));
	Image img_ico_maptools = new Image(omsias_mainwindow.class.getResourceAsStream("/img/icon_maptools.png"));
	Image img_ico_omsixconv = new Image(omsias_mainwindow.class.getResourceAsStream("/img/icon_xconv.png"));
	Image img_ico_streetcre = new Image(omsias_mainwindow.class.getResourceAsStream("/img/icon_streetcreator.png"));
	Image img_ico_objeditp = new Image(omsias_mainwindow.class.getResourceAsStream("/img/icon_objeditp.png"));
	Image img_ico_blender = new Image(omsias_mainwindow.class.getResourceAsStream("/img/icon_blender.png"));
	Image img_omsiaslogo = new Image(omsias_mainwindow.class.getResourceAsStream("/img/omsiaslogo2.0.png"));
	Label lb_chooselang, lb_playeroptions, lb_links, lb_usefuldownload, lb_output, lb_contentfield, lb_omsiwebdisk_url, lb_gitlab_url, lb_omsisdk_download, lb_maptools_download, lb_hofsuite_download, lb_devtools, lb_createdby;
	VBox vb_body, vb_cont_left, vb_cont_left_links, vb_cont_middle, vb_cont_right, vb_footer;
	VBox vb_main_logfdialog;
	HBox hb_headbody, hb_bodycontent, hb_bodycont_omsistart, hb_bodycont_gbpatch, hb_bodycont_logferrorcut, hb_bodycont_patchchanger, hb_bodycont_editor, hb_bodycont_hofsuite, hb_bodycont_maptools, hb_bodycont_xconv, hb_bodycont_streetcre, hb_bodycont_objeditp, hb_bodycont_blender, hb_contentbtns, hb_footer;
	ChoiceBox<String> cb_langchoose, cb_content_choose;
	Separator sh, sf, sr_output, sl_links, sm_ads, slm_vert, smr_vert;
	TextField tf_omsidir;
	TextArea ta_output, ta_content, ta_logfdialog;
	Button btn_chooseomsidir, btn_startomsi, btn_4gbpatch, btn_logferrorcut, btn_patchchanger, btn_starteditor, btn_hofsuite, btn_clearoutput, btn_clearcontent, btn_searchupdates, btn_creditsandchangelog, btn_maptools, btn_omsixconv, btn_streetcre, btn_objeditp, btn_blender, btn_settings;
	Button btn_info_gbpatch, btn_info_logferrorcuttool, btn_info_patchchanger, btn_info_editor, btn_info_hofsuite, btn_info_maptools, btn_info_blender;
	static omsias_func fclass = new omsias_func();
	static omsias_dialogs sdclass = new omsias_dialogs();
	
	//properties config save declarations and initializations
	public static String omsidir = "";
	public static String pathhofsuite = "";
	public static String pathblender = "";
	public static Boolean sett_closeomsias = false;
	public static String locale = "en";
	public static String omsiwebdisk = "https://reboot.omsi-webdisk.de/";
	public static String gitlab = "https://gitlab.com/omsi2/OmsiAS";
	public static String downl_omsisdk = "https://reboot.omsi-webdisk.de/net/attachment/2619-omsi-sdk-tools-1-00-zip/";
	public static String downl_maptools = "https://fellowsfilm.co.uk/downloads/omsi-map-tools.1341/";
	public static String downl_hofsuite = "https://reboot.omsi-webdisk.de/file/773-hof-suite-en-de/";
	
	//
	//
	//	START OF CODE / WINDOW
	//
	//
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage primstage) throws Exception {
		reload();
		
		//Scene
		Scene scene = new Scene(bp, 1250, 700);
		scene.getStylesheets().add(getClass().getResource("styles.css").toString());
		
		//
		//
		//	TOP-ELEMENTS
		//
		//
		iv_header = new ImageView();
		iv_header.setImage(img_header);
		iv_header.setFitWidth(1260);
		iv_header.setFitHeight(100);
		iv_header.setSmooth(true);
		
		//
		//
		//	BODY-ELEMENTS
		//
		//
		vb_body = new VBox();
		hb_headbody = new HBox();
		hb_headbody.setSpacing(10);
		hb_headbody.setPadding(new Insets(5));
		hb_headbody.setAlignment(Pos.CENTER);
		
		tf_omsidir = new TextField();
		tf_omsidir.setFocusTraversable(false);
		tf_omsidir.setStyle("-fx-focus-color: transparent; -fx-faint-focus-color: transparent;");
		tf_omsidir.setEditable(false);
		tf_omsidir.setPrefWidth(300);
		tf_omsidir.setText(omsidir);
		if(tf_omsidir.getText().equals("")) {
			tf_omsidir.setText(omsias_mainwindow.bundl.getString("omsias.function.noomsidirfound"));
		}
		btn_chooseomsidir = new Button(bundl.getString("omsias.omsidir.choose"));
		btn_chooseomsidir.setId("button");
		btn_chooseomsidir.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				omsidir = fclass.filechooserDir();
				tf_omsidir.setText(omsidir);
				fclass.saveProp();
			}
		});
		
		lb_chooselang = new Label(bundl.getString("omsias.language.choose"));
		cb_langchoose = new ChoiceBox<>();
		cb_langchoose.setId("choice_lang");
		cb_langchoose.setPadding(new Insets(2));
		cb_langchoose.setPrefSize(150, 20);
		cb_langchoose.getItems().add(bundl.getString("omsias.language.en"));
		cb_langchoose.getItems().add(bundl.getString("omsias.language.de"));
		cb_langchoose.getItems().add(bundl.getString("omsias.language.nl"));
		cb_langchoose.getItems().add(bundl.getString("omsias.language.fr"));
		//check which language is selected
		if(locale.equals("en")) {
			cb_langchoose.setValue(bundl.getString("omsias.language.en"));
		} else if(locale.equals("de")) {
			cb_langchoose.setValue(bundl.getString("omsias.language.de"));
		} else if(locale.equals("nl")) {
			cb_langchoose.setValue(bundl.getString("omsias.language.nl"));
		} else {
			cb_langchoose.setValue(bundl.getString("omsias.language.fr"));
		}
		cb_langchoose.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			//on language selected
			public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
				if(cb_langchoose.getSelectionModel().getSelectedItem().equals(bundl.getString("omsias.language.en"))) {
					locale = "en";
					fclass.saveProp();
					outputSet(bundl.getString("omsias.output.restartrequiredtouselanguage"));
				} else if(cb_langchoose.getSelectionModel().getSelectedItem().equals(bundl.getString("omsias.language.de"))) {
					locale = "de";
					fclass.saveProp();
					outputSet(bundl.getString("omsias.output.restartrequiredtouselanguage"));
				} else if(cb_langchoose.getSelectionModel().getSelectedItem().equals(bundl.getString("omsias.language.nl"))) {
					locale = "nl";
					fclass.saveProp();
					outputSet(bundl.getString("omsias.output.restartrequiredtouselanguage"));
				} else if(cb_langchoose.getSelectionModel().getSelectedItem().equals(bundl.getString("omsias.language.fr"))) {
					locale = "fr";
					fclass.saveProp();
					outputSet(bundl.getString("omsias.output.restartrequiredtouselanguage"));
				}
			}
		});
		
		sh = new Separator();
		Region rg_head = new Region();
		HBox.setHgrow(rg_head, Priority.ALWAYS); //used for making a region
		hb_headbody.getChildren().addAll(tf_omsidir, btn_chooseomsidir, rg_head, lb_chooselang, cb_langchoose);
		
		
		//body - content
		hb_bodycontent = new HBox();
		hb_bodycontent.setSpacing(10);
		//hb_bodycontent.setPadding(new Insets(15));
		
		//
		//
		//	body - player options
		//
		//
		vb_cont_left = new VBox();
		vb_cont_left.setSpacing(20);
		vb_cont_left.setPadding(new Insets(15));
		vb_cont_left.setPrefWidth(scene.getWidth()/3);
		lb_playeroptions = new Label(bundl.getString("omsias.title.playeroptions"));
		lb_playeroptions.setFont(Font.font("Arial", FontWeight.BOLD, 12));
		
		//content#!
		hb_bodycont_omsistart = new HBox();
		hb_bodycont_omsistart.setSpacing(10);
		hb_bodycont_omsistart.setAlignment(Pos.CENTER_LEFT);
		iv_ico_omsistart = new ImageView();
		iv_ico_omsistart.setImage(img_ico_omsistart);
		iv_ico_omsistart.setFitHeight(40);
		iv_ico_omsistart.setPreserveRatio(true);
		iv_ico_omsistart.setSmooth(true);
		btn_startomsi = new Button(bundl.getString("omsias.button.startomsi"));
		btn_startomsi.setId("button");
		btn_startomsi.setPrefHeight(35);
		btn_startomsi.setPrefWidth(230);
		btn_startomsi.setFont(Font.font("Arial", FontWeight.MEDIUM, 13));
		btn_startomsi.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if(checkOmsiDirChoosen()) {
					//do absolutly nothing
				} else {
					String omsiexe = omsidir+"\\Omsi.exe";
					fclass.launchprogram(omsiexe);
					outputSet(bundl.getString("omsias.output.omsiisstarting"));
					//if option is set to close OmsiAS after launching Omsi ...
					if(sett_closeomsias) {
						System.exit(0);
					}
				}
			}
		});
		hb_bodycont_omsistart.getChildren().addAll(iv_ico_omsistart, btn_startomsi);
		//content#!
		hb_bodycont_gbpatch = new HBox();
		hb_bodycont_gbpatch.setSpacing(10);
		hb_bodycont_gbpatch.setAlignment(Pos.CENTER_LEFT);
		iv_ico_gbpatch = new ImageView();
		iv_ico_gbpatch.setImage(img_ico_gppatch);
		iv_ico_gbpatch.setFitHeight(40);
		iv_ico_gbpatch.setPreserveRatio(true);
		iv_ico_gbpatch.setSmooth(true);
		btn_4gbpatch = new Button(bundl.getString("omsias.button.gbpatch"));
		btn_4gbpatch.setId("button");
		btn_4gbpatch.setPrefHeight(35);
		btn_4gbpatch.setPrefWidth(230);
		btn_4gbpatch.setFont(Font.font("Arial", FontWeight.MEDIUM, 13));
		btn_4gbpatch.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if(checkOmsiDirChoosen()) {
					//do absolutly nothing
				} else {
					//start 4GB-Patch
					if(fclass.fileExists(fclass.getJarLoc()+"\\gbpatch\\4gb_patch.exe")) {
						fclass.openExplorer(fclass.getJarLoc()+"\\gbpatch\\");
					} else {
						outputSet(bundl.getString("omsias.output.gbpatchnotfound"));
					}
				}
			}
		});
		btn_info_gbpatch = new Button(bundl.getString("omsias.button.info"));
		btn_info_gbpatch.setId("buttoninfo");
		btn_info_gbpatch.setFont(Font.font("Arial", FontWeight.MEDIUM, 17));
		btn_info_gbpatch.setPrefHeight(35);
		btn_info_gbpatch.setPrefWidth(35);
		btn_info_gbpatch.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				outputSet(bundl.getString("omsias.output.gbpatchinfo"));
			}
		});
		hb_bodycont_gbpatch.getChildren().addAll(iv_ico_gbpatch, btn_4gbpatch, btn_info_gbpatch);
		//content#!
		hb_bodycont_logferrorcut = new HBox();
		hb_bodycont_logferrorcut.setSpacing(10);
		hb_bodycont_logferrorcut.setAlignment(Pos.CENTER_LEFT);
		iv_ico_logfileerrorcut = new ImageView();
		iv_ico_logfileerrorcut.setImage(img_ico_logferrorcut);
		iv_ico_logfileerrorcut.setFitHeight(40);
		iv_ico_logfileerrorcut.setPreserveRatio(true);
		iv_ico_logfileerrorcut.setSmooth(true);
		btn_logferrorcut = new Button(bundl.getString("omsias.button.logfileerrorcut"));
		btn_logferrorcut.setId("button");
		btn_logferrorcut.setPrefHeight(35);
		btn_logferrorcut.setPrefWidth(230);
		btn_logferrorcut.setFont(Font.font("Arial", FontWeight.MEDIUM, 13));
		btn_logferrorcut.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if(checkOmsiDirChoosen()) {
					//do absolutly nothing	
				} else {
					sdclass.logfiledialog();
				}
			}
		});
		btn_info_logferrorcuttool = new Button(bundl.getString("omsias.button.info"));
		btn_info_logferrorcuttool.setId("buttoninfo");
		btn_info_logferrorcuttool.setFont(Font.font("Arial", FontWeight.MEDIUM, 17));
		btn_info_logferrorcuttool.setPrefHeight(35);
		btn_info_logferrorcuttool.setPrefWidth(35);
		btn_info_logferrorcuttool.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				outputSet(bundl.getString("omsias.output.logifleerrorcut_info"));
			}
		});
		hb_bodycont_logferrorcut.getChildren().addAll(iv_ico_logfileerrorcut, btn_logferrorcut, btn_info_logferrorcuttool);		
		
		//content#!
		hb_bodycont_patchchanger = new HBox();
		hb_bodycont_patchchanger.setSpacing(10);
		hb_bodycont_patchchanger.setAlignment(Pos.CENTER_LEFT);
		iv_ico_patchchanger = new ImageView();
		iv_ico_patchchanger.setImage(img_ico_patchchanger);
		iv_ico_patchchanger.setFitHeight(40);
		iv_ico_patchchanger.setPreserveRatio(true);
		iv_ico_patchchanger.setSmooth(true);
		btn_patchchanger = new Button(bundl.getString("omsias.patchchanger.patchchanger"));
		btn_patchchanger.setId("button");
		btn_patchchanger.setPrefHeight(35);
		btn_patchchanger.setPrefWidth(230);
		btn_patchchanger.setFont(Font.font("Arial", FontWeight.MEDIUM, 13));
		btn_patchchanger.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if(checkOmsiDirChoosen()) {
					//do absolutly nothing
				} else {
					String patchchangerpath = new StringBuilder(omsidir).append("\\OMSI 2 Patch Changer.exe").toString();
					if(fclass.fileExists(patchchangerpath) || patchchangerpath.contains("OMSI 2")) {
						fclass.openExplorer(omsidir);
					} else {
						outputSet(bundl.getString("omsias.patchchanger.notfound"));
					}		
				}
			}
		});
		btn_info_patchchanger = new Button(bundl.getString("omsias.button.info"));
		btn_info_patchchanger.setId("buttoninfo");
		btn_info_patchchanger.setFont(Font.font("Arial", FontWeight.MEDIUM, 17));
		btn_info_patchchanger.setPrefHeight(35);
		btn_info_patchchanger.setPrefWidth(35);
		btn_info_patchchanger.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				outputSet(bundl.getString("omsias.patchchanger.info"));
			}
		});
		hb_bodycont_patchchanger.getChildren().addAll(iv_ico_patchchanger, btn_patchchanger, btn_info_patchchanger);
		//separator for links part
		sl_links = new Separator();
		//start of links part
		lb_links = new Label(bundl.getString("omsias.title.usefullinks"));
		lb_links.setPadding(new Insets(0, 0, 10, 0));
		vb_cont_left_links = new VBox();
		vb_cont_left_links.setSpacing(5);
		lb_omsiwebdisk_url = new Label(bundl.getString("omsias.links.omsiwebdisk"));
		lb_omsiwebdisk_url.setId("link_label");
		lb_omsiwebdisk_url.setMaxWidth(370);
		lb_omsiwebdisk_url.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event arg0) {
				fclass.openurl(omsiwebdisk);
			}
		});
		lb_gitlab_url = new Label(bundl.getString("omsias.links.gitlab"));
		lb_gitlab_url.setId("link_label");
		lb_gitlab_url.setMaxWidth(370);
		lb_gitlab_url.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event arg0) {
				fclass.openurl(gitlab);
			}
		});
		
		//downloads part
		lb_usefuldownload = new Label(bundl.getString("omsias.title.usefuldownloads"));
		lb_usefuldownload.setPadding(new Insets(10, 0, 10, 0));
		
		lb_omsisdk_download = new Label(bundl.getString("omsias.downloads.omsisdk"));
		lb_omsisdk_download.setId("link_label");
		lb_omsisdk_download.setMaxWidth(370);
		lb_omsisdk_download.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event arg0) {
				fclass.openurl(downl_omsisdk);
			}
		});
		lb_maptools_download = new Label(bundl.getString("omsias.downloads.maptools"));
		lb_maptools_download.setId("link_label");
		lb_maptools_download.setMaxWidth(370);
		lb_maptools_download.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event arg0) {
				fclass.openurl(downl_maptools);
			}
		});
		lb_hofsuite_download = new Label(bundl.getString("omsias.downloads.hofsuite"));
		lb_hofsuite_download.setId("link_label");
		lb_hofsuite_download.setMaxWidth(370);
		lb_hofsuite_download.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event arg0) {
				fclass.openurl(downl_hofsuite);
			}
		});
		
		
		vb_cont_left_links.getChildren().addAll(lb_links, lb_omsiwebdisk_url, lb_gitlab_url, lb_usefuldownload, lb_omsisdk_download, lb_maptools_download, lb_hofsuite_download);
		//create Scrollbar
		ScrollPane splinks = new ScrollPane();
		splinks.setId("scrollp");
		splinks.setHbarPolicy(ScrollBarPolicy.NEVER);
		splinks.setContent(vb_cont_left_links);
		
		vb_cont_left.getChildren().addAll(lb_playeroptions, hb_bodycont_omsistart, hb_bodycont_gbpatch, hb_bodycont_logferrorcut, hb_bodycont_patchchanger, sl_links, splinks);
		
		//
		//
		//	body - middle (devs part)
		//
		//
		vb_cont_middle = new VBox();
		vb_cont_middle.setSpacing(20);
		vb_cont_middle.setPadding(new Insets(15));
		vb_cont_middle.setPrefWidth(scene.getWidth()/3);
		
		lb_devtools = new Label(bundl.getString("omsias.title.devoptionsandtools"));
		lb_devtools.setFont(Font.font("Arial", FontWeight.BOLD, 12));
		
		//content#!
		hb_bodycont_editor = new HBox();
		hb_bodycont_editor.setSpacing(10);
		hb_bodycont_editor.setAlignment(Pos.CENTER_LEFT);
		iv_ico_omsieditor = new ImageView();
		iv_ico_omsieditor.setImage(img_ico_editorstart);
		iv_ico_omsieditor.setFitHeight(40);
		iv_ico_omsieditor.setPreserveRatio(true);
		iv_ico_omsieditor.setSmooth(true);
		btn_starteditor = new Button(bundl.getString("omsias.button.startomsieditor"));
		btn_starteditor.setId("button");
		btn_starteditor.setPrefHeight(35);
		btn_starteditor.setPrefWidth(230);
		btn_starteditor.setFont(Font.font("Arial", FontWeight.MEDIUM, 13));
		btn_starteditor.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if(checkOmsiDirChoosen()) {
					//do absolutly nothing
				} else {
					if(fclass.fileExists(omsidir)) {
						String omsieditor = omsidir+"\\Omsi.exe -editor";
						fclass.launchprogramwparams(omsieditor);
						outputSet(bundl.getString("omsias.output.omsieditorstarting"));
					}
				}
			}
		});
		btn_info_editor = new Button(bundl.getString("omsias.button.info"));
		btn_info_editor.setId("buttoninfo");
		btn_info_editor.setFont(Font.font("Arial", FontWeight.MEDIUM, 17));
		btn_info_editor.setPrefHeight(35);
		btn_info_editor.setPrefWidth(35);
		btn_info_editor.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				outputSet(bundl.getString("omsias.output.omsieditor"));
			}
		});
		hb_bodycont_editor.getChildren().addAll(iv_ico_omsieditor, btn_starteditor, btn_info_editor);
		//content#!
		hb_bodycont_hofsuite = new HBox();
		hb_bodycont_hofsuite.setSpacing(10);
		hb_bodycont_hofsuite.setAlignment(Pos.CENTER_LEFT);
		iv_ico_hofsuite = new ImageView();
		iv_ico_hofsuite.setImage(img_ico_hofsuite);
		iv_ico_hofsuite.setFitHeight(40);
		iv_ico_hofsuite.setPreserveRatio(true);
		iv_ico_hofsuite.setSmooth(true);
		btn_hofsuite = new Button(bundl.getString("omsias.button.starthofsuite"));
		btn_hofsuite.setId("button");
		btn_hofsuite.setPrefHeight(35);
		btn_hofsuite.setPrefWidth(230);
		btn_hofsuite.setFont(Font.font("Arial", FontWeight.MEDIUM, 13));
		btn_hofsuite.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if(pathhofsuite.equals("")) {
					outputSet(bundl.getString("omsias.function.hofsuitedirselect_plsselect"));
					sdclass.settingsdialog();
				} else {
					fclass.launchprogram(pathhofsuite);
				}
			}
		});
		btn_info_hofsuite = new Button(bundl.getString("omsias.button.info"));
		btn_info_hofsuite.setId("buttoninfo");
		btn_info_hofsuite.setFont(Font.font("Arial", FontWeight.MEDIUM, 17));
		btn_info_hofsuite.setPrefHeight(35);
		btn_info_hofsuite.setPrefWidth(35);
		btn_info_hofsuite.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				outputSet(bundl.getString("omsias.output.hofsuite"));
			}
		});
		hb_bodycont_hofsuite.getChildren().addAll(iv_ico_hofsuite, btn_hofsuite, btn_info_hofsuite);
		//content#!
		hb_bodycont_maptools = new HBox();
		hb_bodycont_maptools.setSpacing(10);
		hb_bodycont_maptools.setAlignment(Pos.CENTER_LEFT);
		iv_ico_maptools = new ImageView();
		iv_ico_maptools.setImage(img_ico_maptools);
		iv_ico_maptools.setFitHeight(40);
		iv_ico_maptools.setPreserveRatio(true);
		iv_ico_maptools.setSmooth(true);
		btn_maptools = new Button(bundl.getString("omsias.button.startmaptools"));
		btn_maptools.setId("button");
		btn_maptools.setPrefHeight(35);
		btn_maptools.setPrefWidth(230);
		btn_maptools.setFont(Font.font("Arial", FontWeight.MEDIUM, 13));
		btn_maptools.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if(checkOmsiDirChoosen()) {
					//do absolutly nothing
				} else {
					//try to start maptools
					String maptoolspath = new StringBuilder(omsidir).append("\\SDK\\maptools.exe").toString();
					if(fclass.fileExists(maptoolspath) || maptoolspath.contains("OMSI 2")) {
						fclass.launchprogram(maptoolspath);
					} else {
						outputSet(bundl.getString("omsias.output.maptoolsnotfound"));
					}	
				}
			}
		});
		btn_info_maptools = new Button(bundl.getString("omsias.button.info"));
		btn_info_maptools.setId("buttoninfo");
		btn_info_maptools.setFont(Font.font("Arial", FontWeight.MEDIUM, 17));
		btn_info_maptools.setPrefHeight(35);
		btn_info_maptools.setPrefWidth(35);
		btn_info_maptools.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				outputSet(bundl.getString("omsias.output.maptoolsinfo"));
			}
		});
		hb_bodycont_maptools.getChildren().addAll(iv_ico_maptools, btn_maptools, btn_info_maptools);
		//content#!
		hb_bodycont_xconv = new HBox();
		hb_bodycont_xconv.setSpacing(10);
		hb_bodycont_xconv.setAlignment(Pos.CENTER_LEFT);
		iv_ico_omsixconv = new ImageView();
		iv_ico_omsixconv.setImage(img_ico_omsixconv);
		iv_ico_omsixconv.setFitHeight(40);
		iv_ico_omsixconv.setPreserveRatio(true);
		iv_ico_omsixconv.setSmooth(true);
		btn_omsixconv = new Button(bundl.getString("omsias.button.startomsixconv"));
		btn_omsixconv.setId("button");
		btn_omsixconv.setPrefHeight(35);
		btn_omsixconv.setPrefWidth(230);
		btn_omsixconv.setFont(Font.font("Arial", FontWeight.MEDIUM, 13));
		btn_omsixconv.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if(checkOmsiDirChoosen()) {
					//do absolutly nothing
				} else {
					//try to start xconv
					String xconvpath = new StringBuilder(omsidir).append("\\SDK\\OmsiXConv.exe").toString();
					if(fclass.fileExists(xconvpath) || xconvpath.contains("OMSI 2")) {
						fclass.launchprogram(xconvpath);
					} else {
						outputSet(bundl.getString("omsias.output.omsixconvnotfound"));
					}	
				}
			}
		});
		
		hb_bodycont_xconv.getChildren().addAll(iv_ico_omsixconv, btn_omsixconv);
		//content#!
		hb_bodycont_streetcre = new HBox();
		hb_bodycont_streetcre.setSpacing(10);
		hb_bodycont_streetcre.setAlignment(Pos.CENTER_LEFT);
		iv_ico_streetcre = new ImageView();
		iv_ico_streetcre.setImage(img_ico_streetcre);
		iv_ico_streetcre.setFitHeight(40);
		iv_ico_streetcre.setPreserveRatio(true);
		iv_ico_streetcre.setSmooth(true);
		btn_streetcre = new Button(bundl.getString("omsias.button.startstreetcre"));
		btn_streetcre.setId("button");
		btn_streetcre.setPrefHeight(35);
		btn_streetcre.setPrefWidth(230);
		btn_streetcre.setFont(Font.font("Arial", FontWeight.MEDIUM, 13));
		btn_streetcre.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if(checkOmsiDirChoosen()) {
					//do absolutly nothing
				} else {
					//try to start streetcreator
					String streetcreatorpath = new StringBuilder(omsidir).append("\\SDK\\StreetCreator.exe").toString();
					if(fclass.fileExists(streetcreatorpath) || streetcreatorpath.contains("OMSI 2")) {
						fclass.launchprogram(streetcreatorpath);
					} else {
						outputSet(bundl.getString("omsias.output.streetcreatornotfound"));
					}	
				}
			}
		});
		hb_bodycont_streetcre.getChildren().addAll(iv_ico_streetcre, btn_streetcre);
		//content#!
		hb_bodycont_objeditp = new HBox();
		hb_bodycont_objeditp.setSpacing(10);
		hb_bodycont_objeditp.setAlignment(Pos.CENTER_LEFT);
		iv_ico_objeditp = new ImageView();
		iv_ico_objeditp.setImage(img_ico_objeditp);
		iv_ico_objeditp.setFitHeight(40);
		iv_ico_objeditp.setPreserveRatio(true);
		iv_ico_objeditp.setSmooth(true);
		btn_objeditp = new Button(bundl.getString("omsias.button.startobjeditp"));
		btn_objeditp.setId("button");
		btn_objeditp.setPrefHeight(35);
		btn_objeditp.setPrefWidth(230);
		btn_objeditp.setFont(Font.font("Arial", FontWeight.MEDIUM, 13));
		btn_objeditp.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if(checkOmsiDirChoosen()) {
					//do absolutly nothing
				} else {
					//try to start objeditp tool
					String objeditppath = new StringBuilder(omsidir).append("\\SDK\\OmsiObjEditP.exe").toString();
					if(fclass.fileExists(objeditppath) || objeditppath.contains("OMSI 2")) {
						fclass.launchprogram(objeditppath);
					} else {
						outputSet(bundl.getString("omsias.output.objeditpnotfound"));
					}			
				}
			}
		});
		hb_bodycont_objeditp.getChildren().addAll(iv_ico_objeditp, btn_objeditp);
		//content#!
		hb_bodycont_blender = new HBox();
		hb_bodycont_blender.setSpacing(10);
		hb_bodycont_blender.setAlignment(Pos.CENTER_LEFT);
		iv_ico_blender = new ImageView();
		iv_ico_blender.setImage(img_ico_blender);
		iv_ico_blender.setFitHeight(40);
		iv_ico_blender.setPreserveRatio(true);
		iv_ico_blender.setSmooth(true);
		btn_blender = new Button(bundl.getString("omsias.button.blender"));
		btn_blender.setId("button");
		btn_blender.setPrefHeight(35);
		btn_blender.setPrefWidth(230);
		btn_blender.setFont(Font.font("Arial", FontWeight.MEDIUM, 13));
		btn_blender.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if(pathblender.equals("")) {
					outputSet(bundl.getString("omsias.output.chooseblenderexe"));
					sdclass.settingsdialog();
				} else {
					fclass.launchprogram(pathblender);
				}
			}
		});
		btn_info_blender = new Button(bundl.getString("omsias.button.info"));
		btn_info_blender.setId("buttoninfo");
		btn_info_blender.setFont(Font.font("Arial", FontWeight.MEDIUM, 17));
		btn_info_blender.setPrefHeight(35);
		btn_info_blender.setPrefWidth(35);
		btn_info_blender.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				outputSet(bundl.getString("omsias.output.blenderinfo"));
			}
		});
		hb_bodycont_blender.getChildren().addAll(iv_ico_blender, btn_blender, btn_info_blender);
		//separator between dev tools and ads
		sm_ads = new Separator();
		//start of ads area

		
		vb_cont_middle.getChildren().addAll(lb_devtools, hb_bodycont_editor, hb_bodycont_hofsuite, hb_bodycont_maptools, hb_bodycont_xconv, hb_bodycont_streetcre, hb_bodycont_objeditp, hb_bodycont_blender, sm_ads);
		
		//
		//
		//	body - right side - output and information
		//
		//
		vb_cont_right = new VBox();
		vb_cont_right.setSpacing(20);
		vb_cont_right.setPadding(new Insets(15));
		vb_cont_right.setPrefWidth(scene.getWidth()/3);

		lb_output = new Label(bundl.getString("omsias.title.output"));
		ta_output = new TextArea();
		ta_output.setWrapText(true);
		ta_output.setEditable(false);
		ta_output.setMaxWidth(350);
		ta_output.setStyle("-fx-focus-color: transparent; -fx-faint-focus-color: transparent; -fx-highlight-fill: #FFA216;");
		btn_clearoutput = new Button(bundl.getString("omsias.output.clearbtn"));
		btn_clearoutput.setId("button");
		btn_clearoutput.setFont(Font.font("Arial", FontWeight.MEDIUM, 12));
		btn_clearoutput.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				ta_output.clear();
			}
		});
		//separator after output field
		sr_output = new Separator();
		//starting content field
		lb_contentfield = new Label(bundl.getString("omsias.title.contentfield"));
		ta_content = new TextArea();
		ta_content.setWrapText(true);
		ta_content.setEditable(false);
		ta_content.setMaxWidth(350);
		ta_content.setStyle("-fx-focus-color: transparent; -fx-faint-focus-color: transparent; -fx-highlight-fill: #FFA216;");
		//clear buttons for output field and content field
		hb_contentbtns = new HBox();
		hb_contentbtns.setSpacing(10);
		btn_clearcontent = new Button(bundl.getString("omsias.content.clearbtn"));
		btn_clearcontent.setId("button");
		btn_clearcontent.setFont(Font.font("Arial", FontWeight.MEDIUM, 12));
		btn_clearcontent.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				ta_content.clear();
			}
		});
		//choicebox to choice content to be displayed
		cb_content_choose = new ChoiceBox<>();
		cb_content_choose.setId("choice_lang");
		cb_content_choose.setMaxWidth(190);
		cb_content_choose.getItems().add(bundl.getString("omsias.content.sp_maps"));
		cb_content_choose.getItems().add(bundl.getString("omsias.content.sp_vehicles"));
		cb_content_choose.getItems().add(bundl.getString("omsias.content.sp_sceneryobjects"));
		cb_content_choose.getItems().add(bundl.getString("omsias.content.sp_splines"));
		cb_content_choose.getItems().add(bundl.getString("omsias.content.sp_displaycontent"));
		cb_content_choose.setValue(bundl.getString("omsias.content.sp_displaycontent"));
		cb_content_choose.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			//on language selected
			public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
				if(cb_content_choose.getSelectionModel().getSelectedItem().equals(bundl.getString("omsias.content.sp_maps"))) {
					ta_content.clear();
					getsubdirs("maps");
				} else if(cb_content_choose.getSelectionModel().getSelectedItem().equals(bundl.getString("omsias.content.sp_vehicles"))) {
					ta_content.clear();
					getsubdirs("Vehicles");
				} else if(cb_content_choose.getSelectionModel().getSelectedItem().equals(bundl.getString("omsias.content.sp_sceneryobjects"))) {
					ta_content.clear();
					getsubdirs("Sceneryobjects");
				} else if(cb_content_choose.getSelectionModel().getSelectedItem().equals(bundl.getString("omsias.content.sp_splines"))) {
					ta_content.clear();
					getsubdirs("Splines");
				} else {
					ta_content.clear();
					outputSet(bundl.getString("omsias.output.choosecontentplsselect"));
				}
			}
		});
		
		hb_contentbtns.getChildren().addAll(btn_clearcontent, cb_content_choose);
		vb_cont_right.getChildren().addAll(lb_output, ta_output, btn_clearoutput, sr_output,lb_contentfield, ta_content, hb_contentbtns);
		
		//add elements to body
		slm_vert = new Separator();
		slm_vert.setOrientation(Orientation.VERTICAL);
		smr_vert = new Separator();
		smr_vert.setOrientation(Orientation.VERTICAL);
		//hb_bodycontent.getChildren().addAll(vb_cont_left, slm_vert, vb_cont_middle, smr_vert, vb_cont_right);
		hb_bodycontent.getChildren().addAll(vb_cont_left, slm_vert, vb_cont_middle, smr_vert, vb_cont_right);
		vb_body.getChildren().addAll(hb_headbody, sh, hb_bodycontent);
		
		//
		//
		//FOOTER-ELEMENTS
		//
		//
		vb_footer = new VBox();
		sf = new Separator();
		hb_footer = new HBox();
		hb_footer.setSpacing(10);
		hb_footer.setAlignment(Pos.CENTER);
		hb_footer.setPadding(new Insets(5));
		lb_createdby = new Label(bundl.getString("omsias.footer.createdby")+" AkEgo");
		
		btn_settings = new Button(bundl.getString("omsias.button.settings"));
		btn_settings.setId("button");
		btn_settings.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				sdclass.settingsdialog();
			}
		});
		
		btn_creditsandchangelog = new Button(bundl.getString("omsias.footer.creditsachangelog"));
		btn_creditsandchangelog.setId("button");
		btn_creditsandchangelog.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				if(locale.equals("de")) {
					String readmepath = fclass.getJarLoc()+"\\docs\\ReadmeDE.pdf";
					fclass.openurl(readmepath);
				} else {
					String readmepath = fclass.getJarLoc()+"\\docs\\ReadmeEN.pdf";
					fclass.openurl(readmepath);
				}
			}
		});
		
		btn_searchupdates = new Button(bundl.getString("omsias.footer.checkforupdates"));
		btn_searchupdates.setId("button");
		btn_searchupdates.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				outputSet(fclass.onUpdateChecker());
			}
		});
		
		Region rg_footer = new Region();
		HBox.setHgrow(rg_footer, Priority.ALWAYS);
		hb_footer.getChildren().addAll(lb_createdby, rg_footer, btn_settings, btn_creditsandchangelog, btn_searchupdates);
		vb_footer.getChildren().addAll(sf, hb_footer);
		
		//add elements to borderpane
		bp.setTop(iv_header);
		bp.setCenter(vb_body);
		bp.setBottom(vb_footer);
		
		//
		//
		// 	mainwindow initialise
		//
		//
		primstage.setScene(scene);
		primstage.getIcons().add(img_omsiaslogo);
		primstage.setTitle(bundl.getString("omsias.title"));
		primstage.setResizable(false);
		primstage.show();
	}
	
	private void outputSet(String newStr) {
		//method used to get current output text and add new to this
		ta_output.setText(ta_output.getText()+"\n=>"+newStr);
	}
	
	public void reload() {
		//load properties
		fclass.loadProp();
		//language
		curlang = new Locale(locale);
		bundl = ResourceBundle.getBundle("omsias.akego.de.lang", curlang);
	}
	
	private Boolean checkOmsiDirChoosen() {
		Boolean check;
		if(omsidir.equals("") || tf_omsidir.getText().equals(bundl.getString("omsias.function.noomsidirfound"))) {
			outputSet(bundl.getString("omsias.function.chooseomsidirfirst"));
			check = true; //true = check failed (no omsi dir found)
		} else {
			check = false;  //false = check not failed
		}
		return check;
	}
	
	public static void terminate() {
		System.exit(0);
	}
	
	public void getsubdirs(String dirname) {
		File directory = new File(omsidir+"\\"+dirname);
		String[] list = directory.list();

		//list the directory content
		for(int i = 0; i < list.length; i++){
		String DirName = list[i];
		if(ta_content.getText().equals("")) {
			ta_content.setText(DirName);
		} else {
			ta_content.setText(ta_content.getText()+"\n"+DirName);
		}
		}
	}
}