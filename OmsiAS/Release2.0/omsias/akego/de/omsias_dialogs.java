package omsias.akego.de;

import java.util.Optional;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DialogPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.StageStyle;

public class omsias_dialogs {

	private VBox vb_main, vb_main_links, vb_main_logfdialog;
	private Label lb_sethofsuitedir, lb_setblenderpath, lb_stop_aso_omsistart, lb_deletesave, lb_title_paths, lb_title_generalsettings, lb_linksettings_titlelb, lb_linksettings_pathomsisdk, lb_linksettings_pathmaptools,lb_linksettings_pathhofsuite, lb_logfdialog_copyfunc;
	private TextField tf_hofsuitepath, tf_blenderpath, tf_linksettings_omsisdk, tf_linksettings_maptools, tf_linksettings_hofsuite;
	private HBox hb_goptions_stop_aso_omsistart, hb_deletesave, hb_linksettings_omsisdk, hb_linksettings_maptools, hb_linksettings_hofsuite;
	private CheckBox cb_stop_aso_omsistart, cb_deletesave;
	private TextArea ta_logfdialog;
	private Separator s1, s2;
	private String hofsuitepath;
	
	
	public void settingsdialog() {
		Alert al_dialog = new Alert(AlertType.INFORMATION);
		al_dialog.initStyle(StageStyle.UTILITY);
		al_dialog.setTitle(omsias_mainwindow.bundl.getString("omsias.title.omsiassettings"));
		al_dialog.setHeaderText(null);
		al_dialog.setGraphic(null);
		al_dialog.getDialogPane().setMinWidth(500);
		
		ButtonType btn_linksettings = new ButtonType(omsias_mainwindow.bundl.getString("omsias.settings.openlinksettings"));
		ButtonType btn_cancel = new ButtonType(omsias_mainwindow.bundl.getString("omsias.settings.title.buttonclose"));
		al_dialog.getButtonTypes().setAll(btn_linksettings, btn_cancel);
		
		//initialize VBox
		vb_main = new VBox();
		vb_main.setSpacing(10);
		vb_main.setPadding(new Insets(10));
		
		//initialize Content
		lb_title_paths = new Label(omsias_mainwindow.bundl.getString("omsias.settings.title.paths"));
		lb_title_paths.setFont(Font.font("Arial", FontWeight.BOLD, 12));
		lb_title_paths.setPadding(new Insets(5));
		
		//initialize HOFSuite part
		lb_sethofsuitedir = new Label(omsias_mainwindow.bundl.getString("omsias.function.hofsuitedirselect_headtext"));
		tf_hofsuitepath = new TextField();
		tf_hofsuitepath.setEditable(false);
		tf_hofsuitepath.setText(omsias_mainwindow.pathhofsuite);
		tf_hofsuitepath.setStyle("-fx-focus-color: transparent; -fx-faint-focus-color: transparent;");
		tf_hofsuitepath.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event arg0) {
				hofsuitepath = omsias_mainwindow.fclass.filechooserEXE();
				if(hofsuitepath.contains("HOF Suite")) {
					tf_hofsuitepath.setText(omsias_mainwindow.pathhofsuite);
					omsias_mainwindow.pathhofsuite = hofsuitepath;
				} else {
					tf_hofsuitepath.setText(omsias_mainwindow.bundl.getString("omsias.function.hofsuitedirselect_plsselect"));
				}
			}
		});
		s1 = new Separator();
		//initialize Blender part
		lb_setblenderpath = new Label(omsias_mainwindow.bundl.getString("omsias.settings.blenderpath"));
		tf_blenderpath = new TextField();
		tf_blenderpath.setEditable(false);
		tf_blenderpath.setStyle("-fx-focus-color: transparent; -fx-faint-focus-color: transparent;");
		tf_blenderpath.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event arg0) {
				omsias_mainwindow.pathblender = omsias_mainwindow.fclass.filechooserEXE();
			}
		});
		s2 = new Separator();
		//
		lb_title_generalsettings = new Label(omsias_mainwindow.bundl.getString("omsias.settings.title.general"));
		lb_title_generalsettings.setFont(Font.font("Arial", FontWeight.BOLD, 12));
		lb_title_generalsettings.setPadding(new Insets(5));
		//initialize general options
		hb_goptions_stop_aso_omsistart = new HBox();
		hb_goptions_stop_aso_omsistart.setSpacing(25);
		lb_stop_aso_omsistart = new Label(omsias_mainwindow.bundl.getString("omsias.settings.closeomsias"));
		cb_stop_aso_omsistart = new CheckBox();
		cb_stop_aso_omsistart.setId("check");
		cb_stop_aso_omsistart.setSelected(omsias_mainwindow.sett_closeomsias);
		hb_goptions_stop_aso_omsistart.getChildren().addAll(cb_stop_aso_omsistart, lb_stop_aso_omsistart);
		//delte save
		lb_deletesave = new Label(omsias_mainwindow.bundl.getString("omsias.settings.title.deletesave"));
		lb_deletesave.setId("deletesave");
		cb_deletesave = new CheckBox();
		cb_deletesave.setId("check");
		cb_deletesave.setSelected(false);
		hb_deletesave = new HBox();
		hb_deletesave.setSpacing(25);
		hb_deletesave.getChildren().addAll(cb_deletesave, lb_deletesave);
		
		
		//build dialog
		vb_main.getChildren().addAll(lb_title_paths, lb_sethofsuitedir, tf_hofsuitepath, s1, lb_setblenderpath, tf_blenderpath, s2, lb_title_generalsettings, hb_goptions_stop_aso_omsistart, hb_deletesave);
		al_dialog.getDialogPane().setContent(vb_main);
		DialogPane dpane = al_dialog.getDialogPane();
		dpane.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
		dpane.getStyleClass().add("settingsdialog");
		Optional<ButtonType> result = al_dialog.showAndWait();
		if (result.get() == btn_linksettings){
		    settingsdialoglinks();
		} else {
			//check if save would be deleted
			if(cb_deletesave.isSelected()) {
				omsias_mainwindow.fclass.deleteProp();
				omsias_mainwindow.terminate();
			} else {
				//else ...
				//save properties after pressing OK | Get status of checkbox
				if(cb_stop_aso_omsistart.isSelected()) {
					omsias_mainwindow.sett_closeomsias = true;
				} else {
					omsias_mainwindow.sett_closeomsias = false;
				}
				omsias_mainwindow.fclass.saveProp();
			}
		}
	}
	
	//
	//
	//	links settings menu
	//
	//
	public void settingsdialoglinks() {
		Alert links_dialog = new Alert(AlertType.INFORMATION);
		links_dialog.initStyle(StageStyle.UTILITY);
		links_dialog.setTitle(omsias_mainwindow.bundl.getString("omsias.title.omsassettingslinks"));
		links_dialog.setHeaderText(null);
		links_dialog.setGraphic(null);
		links_dialog.getDialogPane().setMinWidth(500);
		ButtonType btn_back_settings = new ButtonType(omsias_mainwindow.bundl.getString("omsias.settings.title.backtosettings"));
		ButtonType btn_cancel_links = new ButtonType(omsias_mainwindow.bundl.getString("omsias.settings.title.buttonclose"));
		links_dialog.getButtonTypes().setAll(btn_back_settings, btn_cancel_links);
		
		//
		//	initialize VBox and main content
		//
		vb_main_links = new VBox();
		vb_main_links.setSpacing(10);
		vb_main_links.setPadding(new Insets(10));
		
		lb_linksettings_titlelb = new Label(omsias_mainwindow.bundl.getString("omsias.settings.title.listoflinks"));
		lb_linksettings_titlelb.setFont(Font.font("Arial", FontWeight.BOLD, 12));
		
		hb_linksettings_omsisdk = new HBox();
		hb_linksettings_omsisdk.setSpacing(10);
		lb_linksettings_pathomsisdk = new Label(omsias_mainwindow.bundl.getString("omsias.settings.linksettings.omsisdk"));
		tf_linksettings_omsisdk = new TextField();
		tf_linksettings_omsisdk.setId("tffield");
		tf_linksettings_omsisdk.setPrefWidth(300);
		tf_linksettings_omsisdk.setStyle("-fx-highlight-fill: #FFA216;");
		tf_linksettings_omsisdk.setText(omsias_mainwindow.downl_omsisdk);
		hb_linksettings_omsisdk.getChildren().addAll(lb_linksettings_pathomsisdk, tf_linksettings_omsisdk);
		
		hb_linksettings_maptools = new HBox();
		hb_linksettings_maptools.setSpacing(10);
		lb_linksettings_pathmaptools = new Label(omsias_mainwindow.bundl.getString("omsias.settings.linksettings.maptools"));
		tf_linksettings_maptools = new TextField();
		tf_linksettings_maptools.setId("tffield");
		tf_linksettings_maptools.setPrefWidth(300);
		tf_linksettings_maptools.setStyle("-fx-highlight-fill: #FFA216;");
		tf_linksettings_maptools.setText(omsias_mainwindow.downl_maptools);
		hb_linksettings_maptools.getChildren().addAll(lb_linksettings_pathmaptools, tf_linksettings_maptools);
		
		hb_linksettings_hofsuite = new HBox();
		hb_linksettings_hofsuite.setSpacing(10);
		lb_linksettings_pathhofsuite = new Label(omsias_mainwindow.bundl.getString("omsias.settings.linksettings.hofsuite"));
		tf_linksettings_hofsuite = new TextField();
		tf_linksettings_hofsuite.setId("tffield");
		tf_linksettings_hofsuite.setPrefWidth(300);
		tf_linksettings_hofsuite.setStyle("-fx-highlight-fill: #FFA216;");
		tf_linksettings_hofsuite.setText(omsias_mainwindow.downl_hofsuite);
		hb_linksettings_hofsuite.getChildren().addAll(lb_linksettings_pathhofsuite, tf_linksettings_hofsuite);
		

		//
		//	build dialog window
		//
		vb_main_links.getChildren().addAll(lb_linksettings_titlelb, hb_linksettings_omsisdk, hb_linksettings_maptools, hb_linksettings_hofsuite);
		links_dialog.getDialogPane().setContent(vb_main_links);
		DialogPane dpane = links_dialog.getDialogPane();
		dpane.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
		dpane.getStyleClass().add("settingsdialog");
		Optional<ButtonType> result = links_dialog.showAndWait();
		if (result.get() == btn_back_settings){
		    settingsdialog();
		} else {
			//save urls
			omsias_mainwindow.downl_omsisdk = tf_linksettings_omsisdk.getText().toString();
			omsias_mainwindow.downl_maptools = tf_linksettings_maptools.getText().toString();
			omsias_mainwindow.downl_hofsuite = tf_linksettings_hofsuite.getText().toString();
		}
	}
	
	public void logfiledialog() {
		String logfpath = omsias_mainwindow.omsidir+"\\logfile.txt";
		String tf_logftext = omsias_mainwindow.fclass.readLogLine(logfpath);
		
		//dialog window initialisation
		Alert logfdialog = new Alert(AlertType.INFORMATION);
		logfdialog.initStyle(StageStyle.UTILITY);
		logfdialog.setTitle(omsias_mainwindow.bundl.getString("omsias.title.logfilecutter"));
		logfdialog.setHeaderText(null);
		logfdialog.setGraphic(null);
		logfdialog.getDialogPane().setPrefWidth(700);
		logfdialog.getDialogPane().setPrefHeight(400);
		ButtonType btn_copy = new ButtonType(omsias_mainwindow.bundl.getString("omsias.output.copy"));
		ButtonType btn_close = new ButtonType(omsias_mainwindow.bundl.getString("omsias.output.dialogclose"));
		logfdialog.getButtonTypes().setAll(btn_copy, btn_close);
		
		//
		//	initialize VBox and main content
		//
		vb_main_logfdialog = new VBox();
		vb_main_logfdialog.setSpacing(10);
		vb_main_logfdialog.setPadding(new Insets(10));
		
		ta_logfdialog = new TextArea();
		ta_logfdialog.setText(tf_logftext);
		ta_logfdialog.setEditable(false);
		ta_logfdialog.setPrefHeight(300);
		ta_logfdialog.setStyle("-fx-focus-color: transparent; -fx-faint-focus-color: transparent;");
		
		lb_logfdialog_copyfunc = new Label(omsias_mainwindow.bundl.getString("omsias.output.logfcopyfunc"));
		lb_logfdialog_copyfunc.setPadding(new Insets(3));
		
		//
		//	build dialog window
		//
		vb_main_logfdialog.getChildren().addAll(ta_logfdialog, lb_logfdialog_copyfunc);
		logfdialog.getDialogPane().setContent(vb_main_logfdialog);
		DialogPane dpane = logfdialog.getDialogPane();
		dpane.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
		dpane.getStyleClass().add("settingsdialog");
		Optional<ButtonType> result = logfdialog.showAndWait();
		if (result.get() == btn_copy){
			Clipboard clipboard = Clipboard.getSystemClipboard();
		    ClipboardContent content = new ClipboardContent();
		    content.putString(tf_logftext);
		    clipboard.setContent(content);
		} else {
			//close
		}
	}
}
